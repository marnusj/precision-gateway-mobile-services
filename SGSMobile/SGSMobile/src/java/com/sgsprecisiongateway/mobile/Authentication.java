package com.sgsprecisiongateway.mobile;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Date;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.soap.*;
import java.util.ArrayList;
import java.util.HashMap;
import com.sgsprecisiongateway.mobile.models.Credentials;
/**
 *
 * @author jacobsb01
 */
@WebService(serviceName = "Authentication")
public class Authentication extends Base{

    
    public Authentication() {
    
    
    }
    
    private final String GET_USER = "SELECT users.username, users.name, users.surname, users.email, users.user_id, password, tenant_users.tenant_id FROM users, tenant_users WHERE users.user_id = tenant_users.user_id AND username = ? AND password = ?";
    private final String GET_TENANT_ID = "SELECT tenant_id FROM tenant_users WHERE user_id = ?";
    private Statement stmt;
    private ResultSet rs;
    private PreparedStatement pstmt;
    private PreparedStatement pstmt2;
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "loginUser")
    public String loginUser(@WebParam(name = "username") String username,@WebParam(name = "password") String password) throws SQLException{
        
        Boolean userExists = false;
        Credentials creds = new Credentials();
        String userId = "";
        
        try {
            
            createSqlConnection();
                pstmt = conn.prepareStatement(GET_USER);
                pstmt.setString(1, username);
                pstmt.setString(2, password);
                rs = pstmt.executeQuery();
                
                while (rs.next()) {
                
                   userId = rs.getString("user_id");
                    
                System.out.println("got user from db");
                
                creds.setUsername(rs.getString("username"));
                creds.setName(rs.getString("name"));
                creds.setSurname(rs.getString("surname"));
                creds.setEmail(rs.getString("email"));
                creds.setUserId(rs.getString("user_id"));
                creds.setPassword(rs.getString("password"));
                creds.setTenantId(rs.getString("tenant_id"));
                creds.setStatus("SUCCESS");
                
                
                userExists = true;
                break;
                
                }
                
            if (!userExists) {
                
                creds.setStatus("FAILURE"); 
                
                }
        }
        catch (SQLException e){
        
        e.printStackTrace();
            
        }
        finally {
        
        releaseResources();
            
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(creds);
        return json;
    }

}
