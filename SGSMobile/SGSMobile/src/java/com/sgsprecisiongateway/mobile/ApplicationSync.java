/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile;

import com.google.gson.Gson;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import com.sgsprecisiongateway.mobile.models.LocationCountResponse;

/**
 *
 * @author jacobsb01
 */
@WebService(serviceName = "ApplicationSync")
public class ApplicationSync extends Base{

    private Statement stmt;
    private ResultSet rs;
    private PreparedStatement pstmt;
    private static final String GET_LOCATION_COUNT = "SELECT count(*) FROM locations WHERE id in (SELECT location_id FROM tenant_locations WHERE tenant_id=?)";
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "countUpdates")
    public String countUpdates(@WebParam(name = "userId") String userId) {
       
        LocationCountResponse locationCountResponse = new LocationCountResponse();       
        
        try {
            int userIdInt = Integer.parseInt(userId);
            if (userIdInt != -1) {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_LOCATION_COUNT);
                
                
                pstmt.setInt(1, Integer.valueOf(userId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    locationCountResponse.setCount(rs.getString("count"));
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
        
        Gson gson = new Gson(); 
        String json = gson.toJson(locationCountResponse);
        return json;
    }
}
