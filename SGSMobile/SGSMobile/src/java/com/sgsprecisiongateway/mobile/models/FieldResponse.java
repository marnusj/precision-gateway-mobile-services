/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "fields")
@XmlAccessorType(XmlAccessType.FIELD) 
public class FieldResponse {
    @XmlElement 
    private ArrayList<Field> fields = new ArrayList<Field>();

    public void setField(Field field) {
        
        this.fields.add(field);
    }
    
}
