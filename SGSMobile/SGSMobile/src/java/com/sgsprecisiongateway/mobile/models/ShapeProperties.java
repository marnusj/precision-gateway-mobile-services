/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "shapeProperty")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShapeProperties {

        @XmlElement
        private String id;
        @XmlElement
        private String name;
        @XmlElement
        private String value;
        @XmlElement
        private String shapeId;
        @XmlElement
        private String shapePropertyTypeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getShapeId() {
        return shapeId;
    }

    public void setShapeId(String shapeId) {
        this.shapeId = shapeId;
    }

    public String getShapePropertyTypeId() {
        return shapePropertyTypeId;
    }

    public void setShapePropertyTypeId(String shapePropertyTypeId) {
        this.shapePropertyTypeId = shapePropertyTypeId;
    }
        
        
        

    
}


