/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "Polygon")
@XmlAccessorType(XmlAccessType.FIELD)
public class Polygon
{
    private OuterBoundaryIs outerBoundaryIs;

    private InnerBoundaryIs[] innerBoundaryIs;
    
    public OuterBoundaryIs getOuterBoundaryIs ()
    {
        return outerBoundaryIs;
    }

    public void setOuterBoundaryIs (OuterBoundaryIs outerBoundaryIs)
    {
        this.outerBoundaryIs = outerBoundaryIs;
    }

    public InnerBoundaryIs[] getInnerBoundaryIs ()
    {
        return innerBoundaryIs;
    }

    public void setInnerBoundaryIs (InnerBoundaryIs[] innerBoundaryIs)
    {
        this.innerBoundaryIs = innerBoundaryIs;
    }

    public void removeInnerBoundary(int count){
        this.innerBoundaryIs[count] = null;
    }
    
    @Override
    public String toString()
    {
        return "ClassPojo [outerBoundaryIs = "+outerBoundaryIs+", innerBoundaryIs = "+innerBoundaryIs+"]";
    }
}