/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "cachedMapData")
@XmlAccessorType(XmlAccessType.FIELD)
public class CachedMapData {
 
    @XmlElement
    private String id;
    @XmlElement
    private String mapShapeData;
    @XmlElement
    private String mapShapeStyle;
    @XmlElement
    private String areaId;
    @XmlElement
    private String shapePropertyTypeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMapShapeData() {
        return mapShapeData;
    }

    public void setMapShapeData(String mapShapeData) {
        this.mapShapeData = mapShapeData;
    }

    public String getMapShapeStyle() {
        return mapShapeStyle;
    }

    public void setMapShapeStyle(String mapShapeStyle) {
        this.mapShapeStyle = mapShapeStyle;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getShapePropertyTypeId() {
        return shapePropertyTypeId;
    }

    public void setShapePropertyTypeId(String shapePropertyTypeId) {
        this.shapePropertyTypeId = shapePropertyTypeId;
    }
    
    
    
}


