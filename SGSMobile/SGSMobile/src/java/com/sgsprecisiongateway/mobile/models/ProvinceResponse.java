/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

/**
 *
 * @author jacobsb01
 */
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "provinces")
@XmlAccessorType(XmlAccessType.FIELD) 
public class ProvinceResponse {

    @XmlElement 
    private ArrayList<Province> provinces = new ArrayList<Province>();

    public void setProvince(Province province) {
        
        this.provinces.add(province);
    }
    
}
