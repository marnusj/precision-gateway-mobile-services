/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "Folder")
@XmlAccessorType(XmlAccessType.FIELD)
public class singleFolder {
    
    
    @XmlElement
    private String name;
    @XmlElement
    private Schema Schema;
    @XmlElement
    private Style[] Style;
    @XmlElement
    private singlePlacemark[] Placemark;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Schema getSchema ()
    {
        return Schema;
    }

    public void setSchema (Schema Schema)
    {
        this.Schema = Schema;
    }

    public Style[] getStyle ()
    {
        return Style;
    }

    public void setStyle (Style[] Style)
    {
        this.Style = Style;
    }

    public singlePlacemark[] getPlacemark ()
    {
        return Placemark;
    }

    public void setPlacemark (singlePlacemark[] Placemark)
    {
        this.Placemark = Placemark;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", Schema = "+Schema+", Style = "+Style+", Placemark = "+Placemark+"]";
    }
    
}
