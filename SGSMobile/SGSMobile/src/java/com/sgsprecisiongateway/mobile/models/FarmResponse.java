/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "farms")
@XmlAccessorType(XmlAccessType.FIELD) 
public class FarmResponse {
    
    @XmlElement 
    private ArrayList<Farm> farms = new ArrayList<Farm>();

    public void setFarm(Farm farm) {
        
        this.farms.add(farm);
    }
    
}
