/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "mapTypes")
@XmlAccessorType(XmlAccessType.FIELD) 
public class MapTypeResponse {
    
    @XmlElement 
    private ArrayList<MapType> mapTypes = new ArrayList<MapType>();

    public void setMapType(MapType mapType) {
        
        this.mapTypes.add(mapType);
    }
    
}
