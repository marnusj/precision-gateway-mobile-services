/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "LinearRing")
@XmlAccessorType(XmlAccessType.FIELD)
public class LinearRing
{
    private String coordinates;

    public String getCoordinates ()
    {
        return coordinates;
    }

    public void setCoordinates (String coordinates)
    {
        this.coordinates = coordinates;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [coordinates = "+coordinates+"]";
    }
}
		