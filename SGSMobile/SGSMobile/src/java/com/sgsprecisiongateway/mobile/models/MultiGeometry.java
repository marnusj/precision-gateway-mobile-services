/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "MultiGeometry")
@XmlAccessorType(XmlAccessType.FIELD)
public class MultiGeometry
{
    private Polygon[] Polygon;

    public Polygon[] getPolygon ()
    {
        return Polygon;
    }

    public void setPolygon (Polygon[] Polygon)
    {
        this.Polygon = Polygon;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Polygon = "+Polygon+"]";
    }
}
		
