/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "Document")
@XmlAccessorType(XmlAccessType.FIELD)
public class Document
{
    @XmlElement
    private Folder Folder;

    public Folder getFolder ()
    {
        return Folder;
    }

    public void setFolder (Folder Folder)
    {
        this.Folder = Folder;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Folder = "+Folder+"]";
    }
}