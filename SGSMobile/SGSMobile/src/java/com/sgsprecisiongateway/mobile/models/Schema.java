/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "Schema")
@XmlAccessorType(XmlAccessType.FIELD)
public class Schema
{
    @XmlAttribute
    private String id;
    @XmlElement
    private SimpleField[] SimpleField;
    @XmlAttribute
    private String name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public SimpleField[] getSimpleField ()
    {
        return SimpleField;
    }

    public void setSimpleField (SimpleField[] SimpleField)
    {
        this.SimpleField = SimpleField;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", SimpleField = "+SimpleField+", name = "+name+"]";
    }
}
