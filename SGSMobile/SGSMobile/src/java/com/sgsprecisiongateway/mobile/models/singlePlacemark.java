/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */

@XmlRootElement(name = "Placemark")
@XmlAccessorType(XmlAccessType.FIELD)
public class singlePlacemark {
    
     private Polygon Polygon;

    private String description;

    private String name;

    private ExtendedData ExtendedData;

    private String styleUrl;

    public Polygon getPolygon ()
    {
        return Polygon;
    }

    public void setPolygon (Polygon Polygon)
    {
        this.Polygon = Polygon;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public ExtendedData getExtendedData ()
    {
        return ExtendedData;
    }

    public void setExtendedData (ExtendedData ExtendedData)
    {
        this.ExtendedData = ExtendedData;
    }

    public String getStyleUrl ()
    {
        return styleUrl;
    }

    public void setStyleUrl (String styleUrl)
    {
        this.styleUrl = styleUrl;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Polygon = "+Polygon+", description = "+description+", name = "+name+", ExtendedData = "+ExtendedData+", styleUrl = "+styleUrl+"]";
    }
    
}
