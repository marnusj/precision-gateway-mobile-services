/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author jacobsb01
 */
public class KmlObject
{
    @XmlElement
    private Kml kml;

    public Kml getKml ()
    {
        return kml;
    }

    public void setKml (Kml kml)
    {
        this.kml = kml;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [kml = "+kml+"]";
    }
}