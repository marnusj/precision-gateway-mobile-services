/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "Style")
@XmlAccessorType(XmlAccessType.FIELD)
public class Style
{

    @XmlAttribute
    private String id;
    @XmlElement
    private PolyStyle PolyStyle;
    @XmlElement
    private LineStyle LineStyle;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public PolyStyle getPolyStyle ()
    {
        return PolyStyle;
    }

    public void setPolyStyle (PolyStyle PolyStyle)
    {
        this.PolyStyle = PolyStyle;
    }

    public LineStyle getLineStyle ()
    {
        return LineStyle;
    }

    public void setLineStyle (LineStyle LineStyle)
    {
        this.LineStyle = LineStyle;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", PolyStyle = "+PolyStyle+", LineStyle = "+LineStyle+"]";
    }
}
