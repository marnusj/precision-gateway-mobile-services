/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "ExtendedData")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtendedData
{
    private SchemaData SchemaData;

    public SchemaData getSchemaData ()
    {
        return SchemaData;
    }

    public void setSchemaData (SchemaData SchemaData)
    {
        this.SchemaData = SchemaData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SchemaData = "+SchemaData+"]";
    }
}
	
