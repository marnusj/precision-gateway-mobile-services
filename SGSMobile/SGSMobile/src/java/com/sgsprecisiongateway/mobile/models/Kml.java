/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "kml", namespace = "http://www.opengis.net/kml/2.2")
@XmlAccessorType(XmlAccessType.FIELD)
public class Kml
{
    private Document Document;

    private String xmlns;

    public Document getDocument ()
    {
        return Document;
    }

    public void setDocument (Document Document)
    {
        this.Document = Document;
    }

    public String getXmlns ()
    {
        return xmlns;
    }

    public void setXmlns (String xmlns)
    {
        this.xmlns = xmlns;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Document = "+Document+", xmlns = "+xmlns+"]";
    }
}