/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "InnerBoundaryIs")
@XmlAccessorType(XmlAccessType.FIELD)
public class InnerBoundaryIs
{
    private LinearRing LinearRing;

    public LinearRing getLinearRing ()
    {
        return LinearRing;
    }

    public void setLinearRing (LinearRing LinearRing)
    {
        this.LinearRing = LinearRing;
    }
    
    @Override
    public String toString()
    {
        return "ClassPojo [LinearRing = "+LinearRing+"]";
    }
}
