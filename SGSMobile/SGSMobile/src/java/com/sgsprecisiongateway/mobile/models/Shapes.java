/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "shape")
@XmlAccessorType(XmlAccessType.FIELD)
public class Shapes {
    
    @XmlElement
    private String id;
    @XmlElement
    private String shapeDefinition;
    @XmlElement
    private String shapeDescription;
    @XmlElement
    private String areaId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShapeDefinition() {
        return shapeDefinition;
    }

    public void setShapeDefinition(String shapeDefinition) {
        this.shapeDefinition = shapeDefinition;
    }

    public String getShapeDescription() {
        return shapeDescription;
    }

    public void setShapeDescription(String shapeDescription) {
        this.shapeDescription = shapeDescription;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }
    
    
}
