/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jacobsb01
 */
@XmlRootElement(name = "SchemaData")
@XmlAccessorType(XmlAccessType.FIELD)
public class SchemaData
{
    private String schemaUrl;

    private SimpleData[] SimpleData;

    public String getSchemaUrl ()
    {
        return schemaUrl;
    }

    public void setSchemaUrl (String schemaUrl)
    {
        this.schemaUrl = schemaUrl;
    }

    public SimpleData[] getSimpleData ()
    {
        return SimpleData;
    }

    public void setSimpleData (SimpleData[] SimpleData)
    {
        this.SimpleData = SimpleData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [schemaUrl = "+schemaUrl+", SimpleData = "+SimpleData+"]";
    }
}
			
