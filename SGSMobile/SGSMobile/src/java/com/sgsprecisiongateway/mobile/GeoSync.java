/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import com.sgsprecisiongateway.mobile.models.Country;
import com.sgsprecisiongateway.mobile.models.CountryResponse;
import com.sgsprecisiongateway.mobile.models.Province;
import com.sgsprecisiongateway.mobile.models.ProvinceResponse;
import com.sgsprecisiongateway.mobile.models.Farm;
import com.sgsprecisiongateway.mobile.models.FarmResponse;
import com.sgsprecisiongateway.mobile.models.Field;
import com.sgsprecisiongateway.mobile.models.FieldResponse;
import com.sgsprecisiongateway.mobile.models.Period;
import com.sgsprecisiongateway.mobile.models.PeriodResponse;
import com.sgsprecisiongateway.mobile.models.MapType;
import com.sgsprecisiongateway.mobile.models.MapTypeResponse;
import com.sgsprecisiongateway.mobile.models.ShapePropertyType;
import com.sgsprecisiongateway.mobile.models.ShapePropertyTypeResponse;
import java.io.StringWriter;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import org.json.simple.JSONObject;
/**
 *
 * @author jacobsb01
 */
@WebService(serviceName = "GeoSync")
public class GeoSync extends Base{
    private HashMap<String, String> shapepropertyTypeMap;
    private static final String GET_MAP_COUNT = "SELECT count(*) FROM shapes, area WHERE area.tenant_id = ? AND area.area_type = 'FIELD'";    
    private static final String GET_TENANT_USER = "SELECT * FROM tenant_users WHERE user_id = ?";
    private static final String GET_INITIAL_LOCATION = "SELECT l.id, l.name FROM locations l, tenant_locations t WHERE t.tenant_id = ? AND t.location_id = l.id and l.parent_id is null";
    private static final String GET_LOCATIONS_FROM_PARENT = "SELECT * FROM locations WHERE parent_id = ?";
    private static final String GET_LOCATIONS_PERIODS = "SELECT distinct period FROM area WHERE location_id = ?";
    private static final String GET_KML_MAP = "SELECT ST_AsKML(ST_CollectionExtract(c.map_shape_data, 3)), c.map_shape_style FROM cached_map_data c, locations l, area a WHERE l.id = ? AND a.location_id = l.id AND a.area_type = ?::area_types AND a.period = ? AND c.area_id = a.id AND c.shape_property_type_id = ? order by ST_Area(c.map_shape_data) DESC;";
    private static final String GET_ANALYSIS_TYPES = "SELECT * FROM shape_property_type WHERE analysis_type = ?";
    private static final String GET_PERIOD_MAP_TYPES = "SELECT distinct area_type FROM area WHERE location_id = ? AND period = ? AND area_type <> 'FIELD'";
    private static final String GET_LAST_UPDATE = "SELECT MAX(last_update) from tenant_locations WHERE tenant_id=?";
    private static final String INSERT_REGISTRATION_INFO = "insert into registrationInformation (nameSurename, company, email, phone, province, country) values (?,?,?,?,?,?)";
    
    private Statement stmt;
    private ResultSet rs;
    private ResultSet rsColor;
    
    private PreparedStatement pstmt;
    /**
     * This is a sample web service operation
     */
    
    @WebMethod(operationName = "getUpdatesAvailable")
    public Boolean getUpdatesAvailable(@WebParam(name = "tenantId") String tenantId,
            @WebParam(name = "timestamp") String timestamp) {
    
        Boolean result = true;
        
        try {
        
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_LAST_UPDATE);
                pstmt.setInt(1, Integer.valueOf(tenantId));
                System.out.println("----------------Checking for time stamp " + timestamp + " on tenantID " + tenantId);
                rs = pstmt.executeQuery();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                //Date clientDate = sdf.parse(timestamp);
                while (rs.next()) {
                    String date = rs.getString(1);
                    String lastUpdate = date;

                    if(lastUpdate.equals(timestamp)){
                        result = false;
                    }
                }
    
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
    
    private void setupList(){
        shapepropertyTypeMap = new HashMap<String, String>();
        shapepropertyTypeMap.put("AS", "As"); //Acid Saturation  / Suurversadiging
        shapepropertyTypeMap.put("B", "B"); //Boron  / Boor
        shapepropertyTypeMap.put("CA", "Ca"); // Calcium / Kalsium
        shapepropertyTypeMap.put("CA_PERC", "Ca %"); //Calcium Percentage / Kalsium Verhouding
        shapepropertyTypeMap.put("CAMG", "CaMg"); //Calcium/Magnesium Ratio
        shapepropertyTypeMap.put("CAMGK", "CaMgK"); //Calcium + Magnesium / Potassium Ratio
        shapepropertyTypeMap.put("KUK", "CEC"); //CEC – Cation Exchange Capacity / Katioon Uitruilings Vermoe
        shapepropertyTypeMap.put("CU", "Cu"); //Copper / Koper
        shapepropertyTypeMap.put("DIGTHEID", "Density"); //Bulk Density / Bruto Digtheid
        shapepropertyTypeMap.put("EX", "Ex"); //Exchangeable Acid / Uitruilbare Suur
        shapepropertyTypeMap.put("FE", "Fe"); //Iron / Yster
        shapepropertyTypeMap.put("K", "K"); //Potassium / Kalium
        shapepropertyTypeMap.put("K_PERC", "K %"); //Potassium Percentage / Kalium verhouding
        shapepropertyTypeMap.put("MG", "Mg"); //Magnesium 
        shapepropertyTypeMap.put("MG_PERC", "Mg %"); //Magnesium Percentage/verhouding
        shapepropertyTypeMap.put("MN", "Mn"); //Manganese / Mangaan
        shapepropertyTypeMap.put("NA", "Na"); //Sodium / Natrium
        shapepropertyTypeMap.put("P_B", "P(Bray 1)"); //Phospate (Bray 1) / Fosfaat (Bray 1)
        shapepropertyTypeMap.put("P", "P(Bray 1)"); //Phospate (Bray 1) / Fosfaat (Bray 1)
        shapepropertyTypeMap.put("P_M", "P(Mehlich 3)"); //Phospate (Mehlich 3) / Fosfaat (Mehlich 3)
        shapepropertyTypeMap.put("PH", "Ph"); //pH (KCL)
        shapepropertyTypeMap.put("S", "S"); //Sulphate / Swael
        shapepropertyTypeMap.put("ZN", "Zn"); //Zinc / Sink
        //SOIL TYPES
        shapepropertyTypeMap.put("CLASS_C", "Soil Type"); // Soil Type
//        shapepropertyTypeMap.put("AVG_M" + period, 46);
//        shapepropertyTypeMap.put("AVG_W" + period, 48);
//        shapepropertyTypeMap.put("AVG_SB" + period, 49);
//        shapepropertyTypeMap.put("AVG_SF" + period, 50);
//        shapepropertyTypeMap.put("AVG_SH" + period, 51);
//        shapepropertyTypeMap.put("AVG_C" + period, 52);
//        shapepropertyTypeMap.put("AVG_B" + period, 53);    
    }
    
    private String getDisplayValue(String value){
        if (shapepropertyTypeMap == null){
            setupList();
        }
        return shapepropertyTypeMap.get(value);
    }
    
    
    @WebMethod(operationName = "getAnalysisPointData")
    public String getAnalysisPointData (@WebParam(name = "mapType") String mapType,@WebParam(name = "period") String period,
            @WebParam(name = "lng") String lng,@WebParam(name = "lat") String lat){

        ShapePropertyTypeResponse shapePropertyTypeResponse = new ShapePropertyTypeResponse();
        ShapePropertyType shapePropertyType = null;
        
        
        try {
            
                createSqlConnection();
                
                stmt = conn.createStatement();
                //ResultSet rs1 = stmt.executeQuery("select area.location_id from shape_properties, shapes, area where area.period = '" + period + "' AND area.id = shapes.area_id AND st_contains(shapes.shape_definition,ST_GeomFromText('POINT(" + lng + " " + lat + ")')) AND shape_properties.shape_id = shapes.id AND shape_properties.shape_property_type_id is not null AND ST_GeometryType(shapes.shape_definition) ILIKE '%polygon%'");
                ResultSet rs1 = stmt.executeQuery("select distinct(area.location_id) from shape_properties, shapes, area where area.id = shapes.area_id AND st_contains(shapes.shape_definition,ST_GeomFromText('POINT(" + lng + " " + lat + ")')) AND shape_properties.shape_id = shapes.id AND shape_properties.shape_property_type_id is not null AND ST_GeometryType(shapes.shape_definition) ILIKE '%polygon%'");
                while (rs1.next()) {
                    Statement stmt1 = conn.createStatement();
                    //ResultSet rs2 = stmt1.executeQuery("select area_type, max(period) as max_period from area where location_id = " + rs1.getInt("location_id") + " and area_type <> 'FIELD' and period <= '" + period + "' group by area_type");
                    ResultSet rs2 = stmt1.executeQuery("select distinct(area_type), max(period) as max_period from area where location_id = " + rs1.getInt("location_id") + " and area_type <> 'FIELD' and area_type <> 'CHEMICAL - SUB SOIL' group by area_type");
                    while (rs2.next()) {
                        Statement stmt2 = conn.createStatement();
                        
                        rs = stmt2.executeQuery("select distinct(shape_properties.name), shape_properties.value from shape_properties, shapes, area where area.period = '" + rs2.getString("max_period") + "' and area_type = '" + rs2.getString("area_type") + "' AND area.id = shapes.area_id AND st_contains(shapes.shape_definition,ST_GeomFromText('POINT(" + lng + " " + lat + ")')) AND shape_properties.shape_id = shapes.id AND shape_properties.shape_property_type_id is not null AND ST_GeometryType(shapes.shape_definition) ILIKE '%polygon%'");
                        while (rs.next()) {
                            //obj.put(rs.getString(1), rs.getString(2));
                            if (getDisplayValue(rs.getString(1)) != null){
                                shapePropertyType = new ShapePropertyType();
                                shapePropertyType.setName(getDisplayValue(rs.getString(1)));
                                shapePropertyType.setValue(rs.getString(2));
                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
                            }
                        }
                    }
            }
            shapePropertyType = new ShapePropertyType();
            shapePropertyType.setName("Lat | Long");
            shapePropertyType.setValue(lat + " | " + lng);
            shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);

 
                
//                stmt = conn.createStatement();
//                rs = stmt.executeQuery("select DISTINCT ON (shape_properties.name) shape_properties.name, shape_properties.value from shape_properties, shapes, area where area.id = shapes.area_id AND st_contains(shapes.shape_definition,ST_GeomFromText('POINT(" + lng + " " + lat + ")')) AND shape_properties.shape_id = shapes.id AND shape_properties.shape_property_type_id is not null AND ST_GeometryType(shapes.shape_definition) ILIKE '%polygon%' order by shape_properties.name, to_number(area.period, '9999')");
//                
//                Boolean p = false;
//                Boolean c = false;
//                Boolean m = false;
//                Boolean k = false;
//                Boolean n = false;
//                Boolean ph = false;
//                Boolean r = false;
//                Boolean s = false;
                
//                while (rs.next()) {
//                    String propertyName = rs.getString(1);
//                    
//                    try {
//                        switch (propertyName.toUpperCase()) {
//                                        
//    
//                        case "P_B": 
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("P(Bray 1)")){
//                                p = true;
//                            }}
//                            
//                            if(!p){
//                                propertyName = "P(Bray 1)";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        case "CA_PERC":  
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("CA %")){
//                                c = true;
//                            }}
//                            if(!c){
//                                propertyName = "CA %";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        case "MG_PERC":  
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("MG %")){
//                                m = true;
//                            }}
//                            if(!m){
//                                propertyName = "MG %";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        case "K_PERC":  
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("K %")){
//                                k = true;
//                            }}
//                            if(!k){
//                                propertyName = "K %";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        case "NA":  
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("NA")){
//                                n = true;
//                            }}
//                            if(!n){
//                                propertyName = "NA";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        case "PH":  
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("PH")){
//                                ph = true;
//                            }}
//                            if(!ph){
//                                propertyName = "PH";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        case "EFFEKTIEWE":  
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("Root Depth")){
//                                r = true;
//                            }}
//                            if(!r){
//                                propertyName = "Root Depth";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        case "CLASS_C":  
//                            for(ShapePropertyType spt : shapePropertyTypeResponse.getShapePropertyType()){
//                            if(spt.getName().equals("Soil Type")){
//                                s = true;
//                            }}
//                            if(!s){
//                                propertyName = "Soil Type";
//                                shapePropertyType = new ShapePropertyType();
//                                shapePropertyType.setName(propertyName); 
//                                shapePropertyType.setValue(rs.getString(2));
//                                shapePropertyTypeResponse.setShapePropertyType(shapePropertyType);
//                            }
//                            break;
//                        default : 
//                    }
//                } catch(Exception e) {
//                    e.printStackTrace();
//                }
//             }
                
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            releaseResources();
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(shapePropertyTypeResponse);
        return json;
}
    
    
   @WebMethod(operationName = "getLocationFromGps")
    public String getLocationFromGps(@WebParam(name = "lat") String lat, 
            @WebParam(name = "lng") String lng ) throws SQLException {
        
        String response = null;
        
        try {
                createSqlConnection();
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT area.location_id FROM shape_properties, shapes, area WHERE area.area_type = 'CHEMICAL' AND area.id = shapes.area_id AND st_contains(shapes.shape_definition,ST_GeomFromText('"+lat+" "+lng+"')) AND shape_properties.shape_id = shapes.id AND shape_properties.shape_property_type_id is not null order by area.period desc limit 1");
                
                while (rs.next()) {
                    response = String.valueOf(rs.getInt(1));
                }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            throw sqlex;
        } finally {
            releaseResources();
        }
        return response;
    } 
    
    @WebMethod(operationName = "getMapTypes")
    public String getMapTypes(@WebParam(name = "userId") String userId, @WebParam(name = "fieldId") String fieldId,
            @WebParam(name = "period") String period) {
    
        MapTypeResponse mapTypeResponse = new MapTypeResponse();
        MapType mapType = null;
        Integer userIdInteger = Integer.parseInt(userId);
        
        try {
            int userIdInt = Integer.parseInt(userId);
            if (userIdInt != -1) {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_PERIOD_MAP_TYPES);
                
                
                pstmt.setInt(1, Integer.valueOf(fieldId));
                pstmt.setString(2, period);
                rs = pstmt.executeQuery();
                
                while (rs.next()) {
                    mapType = new MapType();
                    
                    mapType.setId(fieldId);
                    mapType.setName(rs.getString("area_type"));
                    mapTypeResponse.setMapType(mapType);
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(mapTypeResponse);
        return json;
    }
    
    @WebMethod(operationName = "getPeriods")
    public String getPeriods(@WebParam(name = "userId") String userId, @WebParam(name = "fieldId") String fieldId) {
    
        PeriodResponse periodResponse = new PeriodResponse();
        Period period = null;
        Integer userIdInteger = Integer.parseInt(userId);
        
        try {
            if (userIdInteger != -1) {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_LOCATIONS_PERIODS);
                
                pstmt.setInt(1, Integer.valueOf(fieldId));
                rs = pstmt.executeQuery();
                
                while (rs.next()) {
                    period = new Period();
                    period.setName(rs.getString("period"));
                    periodResponse.setPeriod(period);
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(periodResponse);
        return json;
    }
    
    @WebMethod(operationName = "getFields")
    public String getFields(@WebParam(name = "userId") String userId, @WebParam(name = "farmId") String farmId) {
    
        FieldResponse fieldResponse = new FieldResponse();
        Integer userIdInteger = Integer.parseInt(userId);
        Field field = null;
        
        try {
            if (userIdInteger != -1) {
                
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_LOCATIONS_FROM_PARENT);
                pstmt.setInt(1, Integer.valueOf(farmId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    field = new Field();
                    field.setId(String.valueOf(rs.getInt("id")));
                    field.setName(rs.getString("name"));
                    fieldResponse.setField(field);
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(fieldResponse);
        return json;
    }
    
    @WebMethod(operationName = "getFarms")
    public String getFarms(@WebParam(name = "userId") String userId, @WebParam(name = "provinceId") String provinceId) {
    
        FarmResponse farmResponse = new FarmResponse();
        Farm farm = null;
        Integer userIdInteger = Integer.parseInt(userId);
        
        try {
            if (userIdInteger != -1) {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_LOCATIONS_FROM_PARENT);
                pstmt.setInt(1, Integer.valueOf(provinceId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    farm = new Farm();
                    farm.setId(String.valueOf(rs.getInt("id")));
                    farm.setName(rs.getString("name"));
                    farmResponse.setFarm(farm);
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(farmResponse);
        return json;
    }
    
    @WebMethod(operationName = "getProvinces")
    public String getProvinces(@WebParam(name = "userId") String userId, @WebParam(name = "countryId") String countryId) {
        
        ProvinceResponse provinceResponse = new ProvinceResponse();
        Integer userIdInteger = Integer.parseInt(userId);
        Province province = null;
        
        try {
            if (userIdInteger != -1) {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_LOCATIONS_FROM_PARENT);
                pstmt.setInt(1, Integer.valueOf(countryId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    province = new Province();
                    //response.add(0,String.valueOf(rs.getInt("id")));
                    //response.add(1,rs.getString("name"));
                    province.setId(String.valueOf(rs.getInt("id")));
                    province.setName(rs.getString("name"));
                    provinceResponse.setProvince(province);
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
    
        Gson gson = new Gson(); 
        String json = gson.toJson(provinceResponse);
        
        return json;
    }
    
     @WebMethod(operationName = "getCountries")
    public String getCountries(@WebParam(name = "userId") String userId) {
        
        CountryResponse countryResponse = new CountryResponse();
        Integer userIdInteger = Integer.parseInt(userId);
        Country country = null;
        
        try {
            
            if (userIdInteger != -1) {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_INITIAL_LOCATION);
                pstmt.setInt(1, Integer.valueOf(userId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    country = new Country();
                    //response.add(0,String.valueOf(rs.getInt("id")));
                    //response.add(1,rs.getString("name"));
                    country.setId(String.valueOf(rs.getInt("id")));
                    country.setName(rs.getString("name"));
                    countryResponse.setCountry(country);
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
    
        Gson gson = new Gson(); 
        String json = gson.toJson(countryResponse);
        
        return json;
    }
    
    
    @WebMethod(operationName = "getKMLMap")
    public String getKMLMap(@WebParam(name = "userId") String userId, @WebParam(name = "locationId") String locationId, 
            @WebParam(name = "mapType") String mapType, @WebParam(name = "period") String period, @WebParam(name = "analysisType") String analysisType) {
    
        String xmlString = "";
        String response = null;       
        
        try {
            int userIdInt = Integer.parseInt(userId);
            if (userIdInt != -1) {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_KML_MAP);
                
                pstmt.setInt(1, Integer.valueOf(locationId));
                pstmt.setString(2, mapType);
                pstmt.setString(3, period);
                pstmt.setInt(4, Integer.valueOf(analysisType));
                rsColor = pstmt.executeQuery();
               
                
                try {
                
                    StringWriter stringWriter = new StringWriter();

                    XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();    
                    XMLStreamWriter xMLStreamWriter = xMLOutputFactory.createXMLStreamWriter(stringWriter);
               
                    xMLStreamWriter.writeStartDocument();
                    xMLStreamWriter.writeStartElement("kml");
                    xMLStreamWriter.writeAttribute("xmlns", "http://www.opengis.net/kml/2.2");

                    xMLStreamWriter.writeStartElement("Document");   
                     //xMLStreamWriter.writeAttribute("company", "Ferrari");
                    xMLStreamWriter.writeStartElement("Folder");  
                    xMLStreamWriter.writeStartElement("name");  
                    xMLStreamWriter.writeCharacters("OGRGeoJSON");
                    xMLStreamWriter.writeEndElement();

                      xMLStreamWriter.writeStartElement("Schema");                  
                      xMLStreamWriter.writeAttribute("name", "OGRGeoJSON");
                      xMLStreamWriter.writeAttribute("id", "OGRGeoJSON");
                    
                      xMLStreamWriter.writeStartElement("SimpleField");  
                      xMLStreamWriter.writeAttribute("name", "Name");
                      xMLStreamWriter.writeAttribute("type", "string");
                      xMLStreamWriter.writeEndElement();
                      xMLStreamWriter.writeStartElement("SimpleField");  
                      xMLStreamWriter.writeAttribute("name", "Description");
                      xMLStreamWriter.writeAttribute("type", "string");
                      xMLStreamWriter.writeEndElement();
                      xMLStreamWriter.writeStartElement("SimpleField"); 
                      xMLStreamWriter.writeAttribute("name", "Colour");
                      xMLStreamWriter.writeAttribute("type", "string");
                      xMLStreamWriter.writeEndElement();

                      xMLStreamWriter.writeEndElement();
                
                int alphCount = 0;
                int indexCount = 0;
                      
                String[] alphabet = new String[]{"a","b","c","d","e","f","g","h","i"};
                String[] indexes = new String[]{"#a","#b","#c","#d","#e","#f","#g","#h","#i"};
                      
                while (rsColor.next()) {

                 String color = rsColor.getString("map_shape_style");
                    
                    xMLStreamWriter.writeStartElement("Style"); 
                    xMLStreamWriter.writeAttribute("id", alphabet[alphCount]);
                      xMLStreamWriter.writeStartElement("LineStyle"); 
                      xMLStreamWriter.writeStartElement("color"); 
                    xMLStreamWriter.writeCharacters("00000000");

                      xMLStreamWriter.writeEndElement();
                      xMLStreamWriter.writeEndElement();

                      xMLStreamWriter.writeStartElement("PolyStyle"); 
                      xMLStreamWriter.writeStartElement("color"); 
                     xMLStreamWriter.writeCharacters("FF"+color.substring(1,color.length()));
                      xMLStreamWriter.writeEndElement();
                      xMLStreamWriter.writeEndElement();
            
                     xMLStreamWriter.writeEndElement();
                    
                     alphCount++;
                    
                }
                
                rs = pstmt.executeQuery();

                
                while(rs.next()){
                
                String map = rs.getString("st_askml");
                    
                xMLStreamWriter.writeStartElement("Placemark"); 
                xMLStreamWriter.writeStartElement("name");
                xMLStreamWriter.writeCharacters("#00FF00");
                xMLStreamWriter.writeEndElement();
                xMLStreamWriter.writeStartElement("description");
                xMLStreamWriter.writeCharacters("#00FF00");
                xMLStreamWriter.writeEndElement();

                xMLStreamWriter.writeStartElement("ExtendedData"); 
                xMLStreamWriter.writeStartElement("SchemaData"); 
                xMLStreamWriter.writeAttribute("schemaUrl", "#OGRGeoJSON");
                
                xMLStreamWriter.writeStartElement("SimpleData"); 
                xMLStreamWriter.writeAttribute("name", "Name");
                xMLStreamWriter.writeCharacters("#00FF00");
                xMLStreamWriter.writeEndElement();
                xMLStreamWriter.writeStartElement("SimpleData"); 
                xMLStreamWriter.writeAttribute("name", "Description");
                xMLStreamWriter.writeCharacters("0");
                xMLStreamWriter.writeEndElement();
                xMLStreamWriter.writeStartElement("SimpleData"); 
                xMLStreamWriter.writeAttribute("name", "Colour");
                xMLStreamWriter.writeCharacters("MULTIPOLYGON");
                xMLStreamWriter.writeEndElement();
                
                xMLStreamWriter.writeEndElement();
                xMLStreamWriter.writeEndElement();

                xMLStreamWriter.writeStartElement("styleUrl"); 
                xMLStreamWriter.writeCharacters(indexes[indexCount]);
                xMLStreamWriter.writeEndElement();
                xMLStreamWriter.writeCData(map);

                xMLStreamWriter.writeEndElement();
                
                indexCount++;
                
                }
                
                    xMLStreamWriter.writeEndDocument();

                     xMLStreamWriter.flush();
                     xMLStreamWriter.close();

                     String rawXmlString = stringWriter.getBuffer().toString();

                     stringWriter.close();

                    String noCdata = rawXmlString.replaceAll("<!\\[CDATA\\[", "");
                    xmlString = noCdata.replaceAll("]]>", "");
                    System.out.println(xmlString);
                
                
                } catch (Exception e){
                
                e.printStackTrace();
                    
                }
          
            }
            
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
        return xmlString;
    }
    
    @WebMethod(operationName = "registerInformation")
    public void registerInformation(@WebParam(name = "name") String name, @WebParam(name = "company") String company, @WebParam(name = "email") String email, @WebParam(name = "phone") String phone, @WebParam(name = "provine") String province, @WebParam(name = "country") String country) {
            
        try {
            
                createSqlConnection();
                pstmt = conn.prepareStatement(INSERT_REGISTRATION_INFO);
                pstmt.setString(1, name);
                pstmt.setString(2, company);
                pstmt.setString(3, email);
                pstmt.setString(4, phone);
                pstmt.setString(5, province);
                pstmt.setString(6, country);
                pstmt.executeUpdate();
            
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();
        }
           
    }

}

