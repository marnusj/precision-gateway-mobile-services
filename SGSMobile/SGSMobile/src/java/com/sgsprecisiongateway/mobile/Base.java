/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile;

import com.sgsprecisiongateway.mobile.models.Legend;
import com.sgsprecisiongateway.mobile.models.Kml;
import com.sgsprecisiongateway.mobile.models.Folder;
import com.sgsprecisiongateway.mobile.models.Placemark;
import com.sgsprecisiongateway.mobile.models.Polygon;
import com.sgsprecisiongateway.mobile.models.Document;
import com.sgsprecisiongateway.mobile.models.SimpleField;
import com.sgsprecisiongateway.mobile.models.MultiGeometry;
import com.sgsprecisiongateway.mobile.models.Schema;
import com.sgsprecisiongateway.mobile.models.singleDocument;
import com.sgsprecisiongateway.mobile.models.singleFolder;
import com.sgsprecisiongateway.mobile.models.singleKml;
import com.sgsprecisiongateway.mobile.models.singlePlacemark;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import static jdk.nashorn.internal.runtime.regexp.joni.constants.AsmConstants.S;
import org.sqlite.SQLiteConfig;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 *
 * @author jacobsb01
 */


public class Base {

    protected String databaseName;

    private static final String CREATE_PROPERTIES_TABLE = "CREATE TABLE IF NOT EXISTS properties (name TEXT , value TEXT)";
    private static final String CREATE_CREDENTIALS_TABLE = "CREATE TABLE IF NOT EXISTS credentials (username TEXT,name TEXT,surname TEXT,email TEXT,user_id TEXT NOT NULL PRIMARY KEY,tenant_id TEXT ,password TEXT,remember_me TEXT)";
    private static final String CREATE_LOCATIONS_TABLE = "CREATE TABLE IF NOT EXISTS locations (id TEXT NOT NULL PRIMARY KEY, name TEXT, type TEXT, parent_id TEXT)";
    private static final String CREATE_AREAS_TABLE = "CREATE TABLE IF NOT EXISTS area (id TEXT NOT NULL PRIMARY KEY, description TEXT, original_link TEXT, number_of_shapes TEXT, area_type TEXT, last_modified TEXT, period TEXT, active TEXT, tenant_id TEXT, location_id TEXT, lat_long TEXT)";
    private static final String CREATE_SHAPES_TABLE = "CREATE TABLE IF NOT EXISTS shapes (id TEXT NOT NULL PRIMARY KEY, shape_definition GEOMETRY, shape_description TEXT, area_id TEXT)";
    private static final String CREATE_SHAPE_PROPERTIES = "CREATE TABLE IF NOT EXISTS shape_properties (id TEXT NOT NULL PRIMARY KEY, name TEXT, value TEXT, shape_id TEXT, shape_property_type_id TEXT)";
    private static final String CREATE_CACHED_MAP_DATA_TABLE = "CREATE TABLE IF NOT EXISTS cached_map_data (id TEXT NOT NULL PRIMARY KEY, shape_definition GEOMETRY, map_shape_style TEXT, area_id TEXT, shape_property_type_id TEXT)";
    private static final String CREATE_SHAPE_PROPERTY_TYPE_TABLE = "CREATE TABLE IF NOT EXISTS shape_property_type (id TEXT NOT NULL PRIMARY KEY, name TEXT, analysis_type TEXT)";
    private static final String CREATE_KML_MAP_DATA = "CREATE TABLE IF NOT EXISTS kml_map_data (locationId TEXT, mapType TEXT, period TEXT, analysisType TEXT, kmlMap TEXT)";
    private static final String CREATE_MAP_LEGEND = "CREATE TABLE IF NOT EXISTS map_legend (locationId TEXT, mapType TEXT, period TEXT, analysisType TEXT, value TEXT, color TEXT)";
    private static final String CREATE_LINKED_TENANT = "CREATE TABLE IF NOT EXISTS linked_tenants (tenant_id TEXT, tenent_name TEXT)";
    private static final String CREATE_TENANT_LOCATIONS = "CREATE TABLE IF NOT EXISTS tenant_locations (last_update TEXT)";

    private static final String GET_CREDENTIALS = "SELECT users.username, users.name, users.surname, users.email, users.user_id, password, tenant_users.tenant_id FROM users, tenant_users WHERE users.user_id = tenant_users.user_id AND username = ? AND password = ?";
    private static final String GET_LOCATIONS = "SELECT * FROM locations WHERE id IN (SELECT location_id FROM tenant_locations WHERE tenant_id = ?)";
    private static final String GET_KML_LOCATIONS = "SELECT id from locations WHERE id IN (SELECT location_id FROM tenant_locations WHERE tenant_id = ?) AND type = 'field'";
    private static final String GET_AREAS = "SELECT * FROM area WHERE location_id IN (SELECT location_id FROM tenant_locations WHERE tenant_id = ?)";
    private static final String GET_KML_AREAS = "SELECT area_type, period FROM area WHERE location_id = ?";
    private static final String GET_SHAPES = "SELECT id, st_astext(shape_definition) AS geojson, shape_description, area_id FROM shapes WHERE area_id = ?";
    private static final String GET_SHAPE_PROPERTIES = "SELECT * FROM shape_properties WHERE shape_id IN (SELECT id FROM shapes WHERE area_id = ?)";
    private static final String GET_CACHED_MAP_DATA = "SELECT id, st_astext(map_shape_data) AS geojson, map_shape_style, area_id, shape_property_type_id FROM cached_map_data WHERE area_id = ?";
    private static final String GET_KML_MAP = "SELECT ST_AsKML(ST_CollectionExtract(c.map_shape_data, 3)), c.map_shape_style FROM cached_map_data c, locations l, area a WHERE l.id = ? AND a.location_id = l.id AND a.area_type = ?::area_types AND a.period = ? AND c.area_id = a.id AND c.shape_property_type_id = ?;";
    private static final String GET_SHAPE_PROPERTY_TYPE = "SELECT * FROM shape_property_type";
    private static final String GET_TENANT_USER = "SELECT * FROM tenant_users WHERE user_id = ?";
    private static final String GET_DATABASE_STATUS = "SELECT status from database_provisioning_status WHERE uuid=?";
    private static final String GET_MAP_LEGEND = "select shape_legend_properties.min_value, shape_legend_properties.max_value, shape_legend_properties.colour, shape_legend_properties.text_value, shape_legend.measurement_unit from shape_legend_properties, shape_legend, area_legend, area where area.location_id = ? and area.area_type = ?::area_types and area.period = ? and area_legend.area_id = area.id and shape_legend.id = area_legend.shape_legend_id and shape_legend_properties.shape_legend_id = shape_legend.id and shape_legend.shape_property_type_id = ?";
    private static final String GET_LAST_UPDATE = "SELECT MAX(last_update) as last_update FROM tenant_locations WHERE tenant_id=?";
    private static final String GET_LINKED_TENANTS = "select t.id, t.name from tenant_users tu join tenant t on t.id = tu.tenant_id where user_id = ?";

    private static final String INSERT_PROPERTIES = "INSERT OR IGNORE INTO properties (name, value) VALUES (?,?);";
    private static final String INSERT_CREDENTIALS = "INSERT OR IGNORE INTO credentials (username, name, surname, email, user_id, tenant_id, password, remember_me) VALUES (?,?,?,?,?,?,?,?);";
    private static final String INSERT_LOCATIONS = "INSERT OR IGNORE INTO locations (id, name, type, parent_id) VALUES (?,?,?,?);";
    private static final String INSERT_AREAS = "INSERT OR IGNORE INTO area (id, description, original_link, number_of_shapes, area_type, last_modified, period, active, tenant_id, location_id, lat_long) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
    private static final String INSERT_SHAPES = "INSERT OR IGNORE INTO shapes (id, shape_definition, shape_description, area_id) VALUES (?,ST_GEOMFROMTEXT(?),?,?);";
    private static final String INSERT_SHAPE_PROPERTIES = "INSERT OR IGNORE INTO shape_properties (id, name, value, shape_id, shape_property_type_id) VALUES (?,?,?,?,?)";
    private static final String INSERT_CACHED_MAP_DATA = "INSERT OR IGNORE INTO cached_map_data (id, shape_definition, map_shape_style, area_id, shape_property_type_id) VALUES (?,ST_GEOMFROMTEXT(?),?,?,?)";
    private static final String INSERT_SHAPE_PROPERTY_TYPE = "INSERT OR IGNORE INTO shape_property_type (id, name, analysis_type) VALUES (?,?,?)";
    private static final String INSERT_KML_MAP_DATA = "INSERT OR IGNORE INTO kml_map_data (locationId, mapType, period, analysisType, kmlMap) VALUES (?,?,?,?,?);";
    private static final String INSERT_STATUS = "INSERT INTO database_provisioning_status (uuid, status) VALUES (?,?);";
    private static final String INSERT_LEGEND = "INSERT INTO map_legend (locationId, mapType, period, analysisType, value, color) VALUES (?,?,?,?,?,?);";
    private static final String INSERT_LAST_UPDATE = "INSERT INTO tenant_locations (last_update) VALUES (?);";
    private static final String INSERT_LINKED_TENATS = "INSERT INTO linked_tenants (tenant_id, tenent_name) VALUES (?,?);";

    private static final String UPDATE_STATUS = "UPDATE database_provisioning_status SET status=? WHERE uuid=?";

    private Kml kml;

    private String uuid;
    private static final String SGS_DATA_SOURCE = "jdbc/prec_gateway";
    protected Connection conn;
    protected Connection sqliteConn;
    protected Connection kmlConn;
    protected Connection legendConn;
    private Statement stmt;
    private Statement sqliteStmt;
    private ResultSet rs;
    private ResultSet sqliteRs;
    private ResultSet rsColor;
    private ResultSet rsKml;
    private ResultSet rsLegend;
    private PreparedStatement pstmt;
    private PreparedStatement sqlitePstmt;
    private PreparedStatement kmlPstmt;
    private PreparedStatement legendPstmt;

    SQLiteConfig config = new SQLiteConfig();

    protected void createSqlConnection() throws SQLException {
        if (conn == null) {
            try {
                Context initialContext = new InitialContext();
                DataSource datasource = (DataSource) initialContext.lookup(SGS_DATA_SOURCE);
                if (datasource != null) {
                    conn = datasource.getConnection();
                }
            } catch (NamingException ex) {
                System.out.println("firstNaming");
                ex.printStackTrace();
            }
        }
    }

    protected void releaseResources() {
        if (conn != null) {

            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("FirstNULL");
                e.printStackTrace();
            } finally {
                conn = null;
            }
        }

        if (sqliteConn != null) {

            try {
                sqliteConn.close();
            } catch (SQLException e) {
                System.out.println("secondNULL");
                e.printStackTrace();
            } finally {
                sqliteConn = null;
            }
        }
    }

    protected void createSqliteConnection() throws SQLException {
        if (sqliteConn == null) {
            try {

                Class.forName("org.sqlite.JDBC");

                config.enableLoadExtension(true);

            // create a database connection                ex.printStackTrace();
                sqliteConn = DriverManager.getConnection("jdbc:sqlite:/home/pg_ftp_user/provisioned_databases/" + databaseName,
                        config.toProperties());

                sqliteStmt = sqliteConn.createStatement();
                sqliteStmt.setQueryTimeout(30);
                sqliteStmt.execute("SELECT load_extension('/home/ec2-user/libspatialite-4.3.0a/src/.libs/mod_spatialite.so')");

            } catch (Exception ex) {
                System.out.println("thirdNULL");
                ex.printStackTrace();

            }

        }
    }

    protected void createKmlConnection() throws SQLException {

        if (kmlConn == null) {
            try {
                Context initialContext = new InitialContext();
                DataSource datasource = (DataSource) initialContext.lookup(SGS_DATA_SOURCE);
                if (datasource != null) {
                    kmlConn = datasource.getConnection();
                }
            } catch (NamingException ex) {
                System.out.println("secondNaming");
                ex.printStackTrace();
            }
        }

    }

    protected void createLegendConnection() throws SQLException {

        if (legendConn == null) {
            try {
                Context initialContext = new InitialContext();
                DataSource datasource = (DataSource) initialContext.lookup(SGS_DATA_SOURCE);
                if (datasource != null) {
                    legendConn = datasource.getConnection();
                }
            } catch (NamingException ex) {
                System.out.println("thirdNaming");
                ex.printStackTrace();
            }
        }

    }

    protected void closeLegendConnection() {

        if (legendConn != null) {

            try {
                legendConn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                legendConn = null;
            }
        }
    }

    protected void closeSqliteConnection() {

        if (sqliteConn != null) {
            try {
                sqliteConn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                sqliteConn = null;
            }
        }
    }

    protected void closeKmlConnection() {

        if (kmlConn != null) {
            try {
                kmlConn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                kmlConn = null;
            }
        }
    }

    protected void createStatusEntry(String uuid) {

        try {
            this.uuid = uuid;
            createSqlConnection();
            pstmt = conn.prepareStatement(INSERT_STATUS);
            pstmt.setString(1, uuid);
            pstmt.setString(2, "IN_PROGRESS");

            pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseResources();
        }
    }

    protected void updateStatusEntry(String uuid) {

        try {

            createSqlConnection();
            pstmt = conn.prepareStatement(UPDATE_STATUS);
            pstmt.setString(1, "COMPLETE");
            pstmt.setString(2, uuid);

            pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseResources();
        }
    }

    protected String checkDatabaseAvailability(String uuid) {

        String availability = "IN_PROGRESS";

        try {
            PreparedStatement dAPstmt = null;
            ResultSet dARs = null;
            createSqlConnection();
            dAPstmt = conn.prepareStatement(GET_DATABASE_STATUS);
            dAPstmt.setString(1, uuid);

            dARs = dAPstmt.executeQuery();

            if (dARs.next()) {

                availability = dARs.getString("status");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return availability;

    }

    protected String createSqliteDatabase(String databaseName) throws SQLException {

        String uuid = "";
        Connection con = null;
        Statement stm = null;
        try {

            Class.forName("org.sqlite.JDBC");

            // enabling dynamic extension loading
            // absolutely required by SpatiaLite
            SQLiteConfig conf = new SQLiteConfig();
            conf.enableLoadExtension(true);

            // create a database connection
            con = DriverManager.getConnection("jdbc:sqlite:/home/pg_ftp_user/provisioned_databases/" + databaseName,
                    conf.toProperties());
            stm = con.createStatement();
            stm.setQueryTimeout(30); // set timeout to 30 sec.

            // loading SpatiaLite
            stm.execute("SELECT load_extension('/home/ec2-user/libspatialite-4.3.0a/src/.libs/mod_spatialite.so')");
            stm.execute(CREATE_PROPERTIES_TABLE);
            stm.execute(CREATE_CREDENTIALS_TABLE);
            stm.execute(CREATE_LOCATIONS_TABLE);
            stm.execute(CREATE_AREAS_TABLE);
            stm.execute(CREATE_SHAPES_TABLE);
            stm.execute(CREATE_SHAPE_PROPERTIES);
            stm.execute(CREATE_CACHED_MAP_DATA_TABLE);
            stm.execute(CREATE_SHAPE_PROPERTY_TYPE_TABLE);
            stm.execute(CREATE_KML_MAP_DATA);
            stm.execute(CREATE_MAP_LEGEND);
            stm.execute(CREATE_TENANT_LOCATIONS);
            stm.execute(CREATE_LINKED_TENANT);
            stm.close();
            sqliteConn = con;

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            //con.close();

            releaseResources();

        }

        return uuid;

    }

    public ArrayList createUserLoginResponse(String email,
            String username, String name, String surname,
            String userId, String message) {

        ArrayList<String> response = new ArrayList<String>();

        response.add(email);
        response.add(username);
        response.add(name);
        response.add(surname);
        response.add(userId);
        response.add(message);

        return response;
    }

    public String createLocationsResponse(int id, String name, String type, int parentId) {

        String response = "<id>" + id + "</id>"
                + "<name>" + name + "</name>"
                + "<type>" + type + "</type>"
                + "<parentId>" + parentId + "</parentId>";

        return response;

    }

    public Boolean insertCredentials(String username, String password) {

        Boolean done = false;
        System.out.println("Getting credentials for user" + username + " password " + password);
        try {

            createSqlConnection();
            pstmt = conn.prepareStatement(GET_CREDENTIALS);
            pstmt.setString(1, username);
            pstmt.setString(2, password);

            rs = pstmt.executeQuery();
            createSqliteConnection();
            int userID = -1;
            while (rs.next()) {

                String name = String.valueOf(rs.getString("name"));
                String surname = String.valueOf(rs.getString("surname"));
                String email = String.valueOf(rs.getString("email"));
                String user_id = String.valueOf(rs.getInt("user_id"));
                userID = rs.getInt("user_id");
                String tenant_id = String.valueOf(rs.getInt("tenant_id"));

                sqlitePstmt = sqliteConn.prepareStatement(INSERT_CREDENTIALS);

                sqlitePstmt.setString(1, username);
                sqlitePstmt.setString(2, name);
                sqlitePstmt.setString(3, surname);
                sqlitePstmt.setString(4, email);
                sqlitePstmt.setString(5, String.valueOf(user_id));
                sqlitePstmt.setString(6, String.valueOf(tenant_id));
                sqlitePstmt.setString(7, password);
                sqlitePstmt.setString(8, "0");

            }

            done = sqlitePstmt.execute();
            sqlitePstmt.close();
            pstmt = conn.prepareStatement(GET_LINKED_TENANTS);
            pstmt.setInt(1, userID);

            rs = pstmt.executeQuery();
            createSqliteConnection();

            while (rs.next()) {

                int tenantID = rs.getInt(1);
                String tenantName = String.valueOf(rs.getString(2));

                sqlitePstmt = sqliteConn.prepareStatement(INSERT_LINKED_TENATS);

                sqlitePstmt.setString(1, Integer.toString(tenantID));
                sqlitePstmt.setString(2, tenantName);

                done = sqlitePstmt.execute();
                sqlitePstmt.close();
            }

            

            
            
         //System.out.println("INSERTING CREDENTIALS AT : "+done);
        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            //closeSqliteConnection();
            releaseResources();

        }

        return done;

    }
    

    public Boolean insertLocations(String tenantId) {

        Boolean done = false;
        Integer userIdInteger = Integer.parseInt(tenantId);

        try {

            if (userIdInteger != -1) {

                createSqlConnection();

                pstmt = conn.prepareStatement(GET_LOCATIONS);
                pstmt.setInt(1, Integer.parseInt(tenantId));

                rs = pstmt.executeQuery();

                createSqliteConnection();

                while (rs.next()) {

                    sqlitePstmt = sqliteConn.prepareStatement(INSERT_LOCATIONS);

                    sqlitePstmt.setString(1, String.valueOf(rs.getInt("id")));
                    sqlitePstmt.setString(2, rs.getString("name"));
                    sqlitePstmt.setString(3, rs.getString("type"));
                    sqlitePstmt.setString(4, String.valueOf(rs.getInt("parent_id")));

                    done = sqlitePstmt.execute();
                    sqlitePstmt.close();
                }

                //sqlitePstmt.close();

            }
        } catch (SQLException sqlex) {

            sqlex.printStackTrace();
        } finally {
            //closeSqliteConnection();
            releaseResources();
        }

        return done;

    }

    public Boolean insertMapData(String tenantId) {

        ArrayList<Integer> areaIds = new ArrayList<Integer>();
        Boolean done = false;
        Integer userIdInteger = Integer.parseInt(tenantId);

        try {

            if (userIdInteger != -1) {

                createSqlConnection();

                pstmt = conn.prepareStatement(GET_AREAS);
                pstmt.setInt(1, Integer.parseInt(tenantId));

                rs = pstmt.executeQuery();

                createSqliteConnection();

                while (rs.next()) {

                    try {

                        sqlitePstmt = sqliteConn.prepareStatement(INSERT_AREAS);

                        Integer areaId = rs.getInt("id");
                        sqlitePstmt.setString(1, String.valueOf(areaId));
                        sqlitePstmt.setString(2, rs.getString("description"));
                        sqlitePstmt.setString(3, rs.getString("original_link"));
                        sqlitePstmt.setString(4, String.valueOf(rs.getInt("number_of_shapes")));
                        sqlitePstmt.setString(5, rs.getString("area_type"));
                        sqlitePstmt.setString(6, rs.getString("last_modified"));
                        sqlitePstmt.setString(7, rs.getString("period"));
                        sqlitePstmt.setString(8, rs.getString("active"));
                        sqlitePstmt.setString(9, String.valueOf(rs.getInt("tenant_id")));
                        sqlitePstmt.setString(10, String.valueOf(rs.getInt("location_id")));
                        sqlitePstmt.setString(11, rs.getString("lat_long"));

                        done = sqlitePstmt.execute();
                        sqlitePstmt.close();
                        areaIds.add(areaId);

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

                //sqlitePstmt.close();

            }
        } catch (SQLException sqlex) {

            sqlex.printStackTrace();

        } finally {
            //closeSqliteConnection();
            releaseResources();

        }

        try {

            createSqlConnection();

            pstmt = conn.prepareStatement(GET_SHAPE_PROPERTY_TYPE);

            rs = pstmt.executeQuery();

            try {

                createSqliteConnection();

                while (rs.next()) {

                    try {

                        sqlitePstmt = sqliteConn.prepareStatement(INSERT_SHAPE_PROPERTY_TYPE);

                        Integer typeId = rs.getInt("id");

                        sqlitePstmt.setString(1, String.valueOf(typeId));
                        sqlitePstmt.setString(2, rs.getString("name"));
                        sqlitePstmt.setString(3, rs.getString("analysis_type"));

                        done = sqlitePstmt.execute();
                        sqlitePstmt.close();
                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

            } catch (Exception e) {

                e.printStackTrace();

            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            //closeSqliteConnection();
            releaseResources();

        }

        return done;

    }

    public void prepareKmlMapData(String tenantId) {
        Boolean done = false;

        ArrayList<String> analysisTypeIds = new ArrayList<String>();

        int tenantIdInt = Integer.parseInt(tenantId);
        if (tenantIdInt != -1) {

            try {

                createSqlConnection();

                pstmt = conn.prepareStatement(GET_SHAPE_PROPERTY_TYPE);

                rs = pstmt.executeQuery();

                while (rs.next()) {

                    try {

                        analysisTypeIds.add(String.valueOf(rs.getInt("id")));

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

                //pstmt.close();
            } catch (Exception e) {

                e.printStackTrace();

            } finally {

                releaseResources();

            }

        }

        try {

            if (tenantIdInt != -1) {

                try {

                    createSqlConnection();
                    
                    PreparedStatement p = null;
                    ResultSet r = null;
                    PreparedStatement kmlPs = null;
                    pstmt = conn.prepareStatement(GET_KML_LOCATIONS);
                    pstmt.setInt(1, Integer.valueOf(tenantId));

                    rs = pstmt.executeQuery();

                    while (rs.next()) {

                        try {

                            Integer locationId = rs.getInt("id");

                            p = conn.prepareStatement(GET_KML_AREAS);
                            p.setInt(1, locationId);
                            System.out.println(locationId +" : "+tenantId);

                            r = p.executeQuery();

                            while (r.next()) {

                                for (String aType : analysisTypeIds) {

                                    try {
                                        createSqliteConnection();
                                        String areaType = r.getString("area_type");
                                        String period = r.getString("period");

                       //SELECT KML HERE
                                        ArrayList<Legend> legends = createLegends(String.valueOf(locationId), areaType, period, aType);
                                        String kml = getKMLMap(tenantId, String.valueOf(locationId), areaType, period, aType);

                                        if (kml.contains("<MultiGeometry>")) {

                                            JAXBContext jaxbContext = JAXBContext.newInstance(Kml.class);
                                            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                                            ArrayList<Kml> kmlMaps = splitKmlLayer(kml);

                                            for (Kml kmlMap : kmlMaps) {

                                                StringWriter sw = new StringWriter();
                                                jaxbMarshaller.marshal(kmlMap, sw);
                                                String kmlString = sw.toString();

                                                kmlPs = sqliteConn.prepareStatement(INSERT_KML_MAP_DATA);
                                                kmlPs.setString(1, String.valueOf(locationId));
                                                kmlPs.setString(2, areaType);
                                                kmlPs.setString(3, period);
                                                kmlPs.setString(4, aType);
                                                kmlPs.setString(5, kmlString);       // TODO create string marshaller

                                                done = kmlPs.execute();
                                                kmlPs.close();
                                            }

                                        } else if (kml.contains("<Polygon>")) {

                                            JAXBContext jaxbContext = JAXBContext.newInstance(singleKml.class);
                                            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                                            ArrayList<singleKml> kmlMaps = splitSingleKmlLayer(kml);

                                            for (singleKml kmlMap : kmlMaps) {

                                                StringWriter sw = new StringWriter();
                                                jaxbMarshaller.marshal(kmlMap, sw);
                                                String kmlString = sw.toString();

                                                kmlPs = sqliteConn.prepareStatement(INSERT_KML_MAP_DATA);
                                                kmlPs.setString(1, String.valueOf(locationId));
                                                kmlPs.setString(2, areaType);
                                                kmlPs.setString(3, period);
                                                kmlPs.setString(4, aType);
                                                kmlPs.setString(5, kmlString);       // TODO create string marshaller

                                                done = kmlPs.execute();
                                                kmlPs.close();
                                            }

                                        }
                       //INSERT KML HERE

                                        for (Legend legend : legends) {

                                            kmlPs = sqliteConn.prepareStatement(INSERT_LEGEND);
                                            kmlPs.setString(1, String.valueOf(locationId));
                                            kmlPs.setString(2, areaType);
                                            kmlPs.setString(3, period);
                                            kmlPs.setString(4, aType);
                                            kmlPs.setString(5, legend.getValue());
                                            kmlPs.setString(6, legend.getColor());

                                            done = kmlPs.execute();
                                            kmlPs.close();
                                        }

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    } finally {
                                    
                                        try {
                                            if (kmlPs != null) {
                                                kmlPs.close();
                                                kmlPs = null;
                                            }

                                        } catch (Exception e) {
                                                
                                        e.printStackTrace();
                                        
                                        }
                                                                                //closeSqliteConnection();
                                    
                                    }

                                }

                            }
                            p.close();
                            r.close();
                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    }

                    //pstmt.close();
                } catch (Exception e) {

                    e.printStackTrace();

                } finally {

                    

                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            releaseResources();
            
        }

    }

    public ArrayList<singleKml> splitSingleKmlLayer(String kmlMap) {

        ArrayList<singleKml> kmlList = new ArrayList();

        try {

            ArrayList<String> coordinateList;
            int index = 0;
            singlePlacemark[] placemarks = null;

            JAXBContext jaxbContext = JAXBContext.newInstance(singleKml.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(kmlMap);

            singleKml kml = (singleKml) unmarshaller.unmarshal(reader);
            placemarks = kml.getDocument().getFolder().getPlacemark();

            HashMap<Integer, ArrayList<String>> coordinateMap = new HashMap();
            ArrayList<Integer> outerBoundaryPolygons = new ArrayList();

            for (singlePlacemark p : placemarks) {

                Polygon poly = null;

                poly = p.getPolygon();

                coordinateList = new ArrayList();

                Boolean hasInnerBoundary = false;

                try {

                    if (poly.getInnerBoundaryIs() != null) {

                        hasInnerBoundary = true;
                        for (int i = 0; i < poly.getInnerBoundaryIs().length; i++) {

                            String[] coordinates = poly.getInnerBoundaryIs()[i].getLinearRing().getCoordinates().split(" ");

                            for (int k = 0; k < 1; k++) {
                                //System.out.println(i);
                                coordinateList.add(coordinates[0]);

                            }
                        }

                    }

                } catch (Exception e) {
                    System.out.println("Null incoming");
                    e.printStackTrace();

                }

                if (!hasInnerBoundary) {

                    outerBoundaryPolygons.add(index);

                }

                coordinateMap.put(index, coordinateList);
                index++;

            }

            index = 0;
            ArrayList<Integer> innerBoundaryPolygons = new ArrayList();

            for (singlePlacemark p : placemarks) {

                Polygon poly = p.getPolygon();
                int count = 1;

                try {

                    for (int i = 0; i < coordinateMap.size(); i++) {
                        for (String coordinate : coordinateMap.get(i)) {

                            if (poly.getOuterBoundaryIs().getLinearRing().getCoordinates().contains(coordinate)) {

                                //System.out.println(index+" : "+coordinate+" : true "+i);
                                count++;

                                if (!innerBoundaryPolygons.isEmpty()) {

                                    if (!innerBoundaryPolygons.contains(i)) {

                                        innerBoundaryPolygons.add(i);

                                    }
                                } else {

                                    innerBoundaryPolygons.add(i);

                                }
                            }
                        }
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }

                index++;
            //System.out.println(count);

            }

            for (int I : innerBoundaryPolygons) {

                //System.out.println(I);
                singleKml newKml = new singleKml();
                singleDocument d = new singleDocument();
                singleFolder f = new singleFolder();

                f.setName("OGRGeoJSON");

                SimpleField spf1 = new SimpleField();
                spf1.setName("Name");
                spf1.setType("string");

                SimpleField spf2 = new SimpleField();
                spf2.setName("Description");
                spf2.setType("string");

                SimpleField spf3 = new SimpleField();
                spf3.setName("Colour");
                spf3.setType("string");

                SimpleField[] simpleArray = new SimpleField[]{spf1, spf2, spf3};

                Schema sch = new Schema();
                sch.setSimpleField(simpleArray);
                f.setSchema(sch);
                f.setStyle(kml.getDocument().getFolder().getStyle());

                singlePlacemark placemark = kml.getDocument().getFolder().getPlacemark()[I];
                Polygon m = placemark.getPolygon();
                int inCount = 0;

                m.setInnerBoundaryIs(null);
                inCount++;

                placemark.setPolygon(m);

                singlePlacemark[] placemarkArray = new singlePlacemark[]{placemark};

                f.setPlacemark(placemarkArray);
                d.setFolder(f);
                newKml.setDocument(d);

                kmlList.add(newKml);
            //System.out.println(placemark.toString());

            }

            for (int E : outerBoundaryPolygons) {

                //System.out.println(E);
                singleKml newKml = new singleKml();
                singleDocument d = new singleDocument();
                singleFolder f = new singleFolder();
                f.setName("OGRGeoJSON");

                SimpleField spf1 = new SimpleField();
                spf1.setName("Name");
                spf1.setType("string");

                SimpleField spf2 = new SimpleField();
                spf2.setName("Description");
                spf2.setType("string");

                SimpleField spf3 = new SimpleField();
                spf3.setName("Colour");
                spf3.setType("string");

                SimpleField[] simpleArray = new SimpleField[]{spf1, spf2, spf3};

                Schema sch = new Schema();
                sch.setSimpleField(simpleArray);
                f.setSchema(sch);
                f.setStyle(kml.getDocument().getFolder().getStyle());
                singlePlacemark placemark = kml.getDocument().getFolder().getPlacemark()[E];
                Polygon m = placemark.getPolygon();
                int inCount = 0;

                m.setInnerBoundaryIs(null);
                inCount++;

                placemark.setPolygon(m);

                singlePlacemark[] placemarkArray = new singlePlacemark[]{placemark};

                f.setPlacemark(placemarkArray);
                d.setFolder(f);
                newKml.setDocument(d);
                //System.out.println(newKml);
                kmlList.add(newKml);

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return kmlList;

    }

    public ArrayList<Kml> splitKmlLayer(String kmlMap) {

        ArrayList<Kml> kmlList = new ArrayList();

        try {
            ArrayList<String> coordinateList;
            int index = 0;
            Placemark[] placemarks = null;

            JAXBContext jaxbContext = JAXBContext.newInstance(Kml.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(kmlMap);

            kml = (Kml) unmarshaller.unmarshal(reader);
            placemarks = kml.getDocument().getFolder().getPlacemark();

            HashMap<Integer, ArrayList<String>> coordinateMap = new HashMap();
            ArrayList<Integer> outerBoundaryPolygons = new ArrayList();

            for (Placemark p : placemarks) {

                Polygon[] poly = null;

                try {

                    poly = p.getMultiGeometry().getPolygon();

                } catch (Exception e) {

                    poly = p.getPolygon();

                }
                coordinateList = new ArrayList();

                Boolean hasInnerBoundary = false;
                for (Polygon pol : poly) {

                    try {

                        if (pol.getInnerBoundaryIs() != null) {

                            hasInnerBoundary = true;
                            for (int i = 0; i < pol.getInnerBoundaryIs().length; i++) {

                                String[] coordinates = pol.getInnerBoundaryIs()[i].getLinearRing().getCoordinates().split(" ");

                                for (int k = 0; k < 1; k++) {
                                    //System.out.println(i);
                                    coordinateList.add(coordinates[0]);

                                }
                            }

                        }

                    } catch (Exception e) {
                        System.out.println("Null incoming");
                        e.printStackTrace();

                    }

                }

                if (!hasInnerBoundary) {

                    outerBoundaryPolygons.add(index);

                }

                coordinateMap.put(index, coordinateList);
                index++;

            }

            index = 0;
            ArrayList<Integer> innerBoundaryPolygons = new ArrayList();

            for (Placemark p : placemarks) {

                Polygon[] poly = null;

                try {

                    poly = p.getMultiGeometry().getPolygon();

                } catch (Exception e) {

                    poly = p.getPolygon();

                }
                int count = 1;

                for (Polygon pol : poly) {

                    try {

                        for (int i = 0; i < coordinateMap.size(); i++) {
                            for (String coordinate : coordinateMap.get(i)) {

                                if (pol.getOuterBoundaryIs().getLinearRing().getCoordinates().contains(coordinate)) {

                                    //System.out.println(index+" : "+coordinate+" : true "+i);
                                    count++;

                                    if (!innerBoundaryPolygons.isEmpty()) {

                                        if (!innerBoundaryPolygons.contains(i)) {

                                            innerBoundaryPolygons.add(i);

                                        }
                                    } else {

                                        innerBoundaryPolygons.add(i);

                                    }
                                }
                            }
                        }

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

                index++;
            //System.out.println(count);

            }

            for (int I : innerBoundaryPolygons) {

                //System.out.println(I);
                Kml newKml = new Kml();
                Document d = new Document();
                Folder f = new Folder();

                f.setName("OGRGeoJSON");

                SimpleField spf1 = new SimpleField();
                spf1.setName("Name");
                spf1.setType("string");

                SimpleField spf2 = new SimpleField();
                spf2.setName("Description");
                spf2.setType("string");

                SimpleField spf3 = new SimpleField();
                spf3.setName("Colour");
                spf3.setType("string");

                SimpleField[] simpleArray = new SimpleField[]{spf1, spf2, spf3};

                Schema sch = new Schema();
                sch.setSimpleField(simpleArray);
                f.setSchema(sch);
                f.setStyle(kml.getDocument().getFolder().getStyle());

                Placemark placemark = kml.getDocument().getFolder().getPlacemark()[I];
                MultiGeometry m = placemark.getMultiGeometry();
                Polygon[] p = null;

                try {

                    p = m.getPolygon();

                } catch (Exception e) {

                    p = placemark.getPolygon();

                }

                int inCount = 0;

                for (Polygon pol : p) {

                    pol.setInnerBoundaryIs(null);
                    inCount++;

                }

                try {

                    m.setPolygon(p);
                    placemark.setMultiGeometry(m);

                } catch (Exception e) {

                    placemark.setPolygon(p);

                }

                Placemark[] placemarkArray = new Placemark[]{placemark};

                f.setPlacemark(placemarkArray);
                d.setFolder(f);
                newKml.setDocument(d);

                kmlList.add(newKml);
            //System.out.println(placemark.toString());

            }

            for (int E : outerBoundaryPolygons) {

                //System.out.println(E);
                Kml newKml = new Kml();
                Document d = new Document();
                Folder f = new Folder();
                f.setName("OGRGeoJSON");

                SimpleField spf1 = new SimpleField();
                spf1.setName("Name");
                spf1.setType("string");

                SimpleField spf2 = new SimpleField();
                spf2.setName("Description");
                spf2.setType("string");

                SimpleField spf3 = new SimpleField();
                spf3.setName("Colour");
                spf3.setType("string");

                SimpleField[] simpleArray = new SimpleField[]{spf1, spf2, spf3};

                Schema sch = new Schema();
                sch.setSimpleField(simpleArray);
                f.setSchema(sch);
                f.setStyle(kml.getDocument().getFolder().getStyle());
                Placemark placemark = kml.getDocument().getFolder().getPlacemark()[E];
                MultiGeometry m = placemark.getMultiGeometry();
                Polygon[] p = null;

                try {

                    p = m.getPolygon();

                } catch (Exception e) {

                    p = placemark.getPolygon();

                }

                int inCount = 0;

                for (Polygon pol : p) {

                    pol.setInnerBoundaryIs(null);
                    inCount++;

                }

                try {

                    m.setPolygon(p);
                    placemark.setMultiGeometry(m);

                } catch (Exception e) {

                    placemark.setPolygon(p);

                }

                Placemark[] placemarkArray = new Placemark[]{placemark};

                f.setPlacemark(placemarkArray);
                d.setFolder(f);
                newKml.setDocument(d);
                //System.out.println(newKml);
                kmlList.add(newKml);

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return kmlList;

    }

    public ArrayList<Legend> createLegends(String locationId, String mapType, String period, String analysisType) {

        ArrayList<Legend> legends = new ArrayList<Legend>();

        try {

            createLegendConnection();
            legendPstmt = legendConn.prepareStatement(GET_MAP_LEGEND);
            legendPstmt.setInt(1, Integer.valueOf(locationId));
            legendPstmt.setString(2, mapType);
            legendPstmt.setString(3, period);
            legendPstmt.setInt(4, Integer.valueOf(analysisType));
            rsLegend = legendPstmt.executeQuery();

            while (rsLegend.next()) {

                Legend legendField = new Legend();
                if (rsLegend.getString(4) == null || rsLegend.getString(4).equalsIgnoreCase("")) {
                    String measurementUnit = "";
                    if (rsLegend.getString(5) != null && !rsLegend.getString(5).equalsIgnoreCase("")) {
                        measurementUnit = rsLegend.getString(5);
                    }
                    if (!rsLegend.getString(2).equalsIgnoreCase("1000000")) {
                        legendField.setValue(rsLegend.getString(1) + " - " + rsLegend.getString(2) + " " + measurementUnit);
                    } else {
                        legendField.setValue("> " + rsLegend.getString(1) + " " + measurementUnit);
                    }
                } else {
                    legendField.setValue(rsLegend.getString(4));
                }
                legendField.setColor(rsLegend.getString(3));

                legends.add(legendField);

            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            closeLegendConnection();

        }
        return legends;

    }

    public String getKMLMap(String userId, String locationId,
            String mapType, String period, String analysisType) {

        String xmlString = "";
        String response = null;

        try {
            int userIdInt = Integer.parseInt(userId);
            if (userIdInt != -1) {
                createKmlConnection();
                kmlPstmt = kmlConn.prepareStatement(GET_KML_MAP);

                kmlPstmt.setInt(1, Integer.valueOf(locationId));
                kmlPstmt.setString(2, mapType);
                kmlPstmt.setString(3, period);
                kmlPstmt.setInt(4, Integer.valueOf(analysisType));
                rsColor = kmlPstmt.executeQuery();

                try {

                    StringWriter stringWriter = new StringWriter();

                    XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
                    XMLStreamWriter xMLStreamWriter = xMLOutputFactory.createXMLStreamWriter(stringWriter);

                    xMLStreamWriter.writeStartDocument();
                    xMLStreamWriter.writeStartElement("kml");
                    xMLStreamWriter.writeAttribute("xmlns", "http://www.opengis.net/kml/2.2");

                    xMLStreamWriter.writeStartElement("Document");
                    //xMLStreamWriter.writeAttribute("company", "Ferrari");
                    xMLStreamWriter.writeStartElement("Folder");
                    xMLStreamWriter.writeStartElement("name");
                    xMLStreamWriter.writeCharacters("OGRGeoJSON");
                    xMLStreamWriter.writeEndElement();

                    xMLStreamWriter.writeStartElement("Schema");
                    xMLStreamWriter.writeAttribute("name", "OGRGeoJSON");
                    xMLStreamWriter.writeAttribute("id", "OGRGeoJSON");

                    xMLStreamWriter.writeStartElement("SimpleField");
                    xMLStreamWriter.writeAttribute("name", "Name");
                    xMLStreamWriter.writeAttribute("type", "string");
                    xMLStreamWriter.writeEndElement();
                    xMLStreamWriter.writeStartElement("SimpleField");
                    xMLStreamWriter.writeAttribute("name", "Description");
                    xMLStreamWriter.writeAttribute("type", "string");
                    xMLStreamWriter.writeEndElement();
                    xMLStreamWriter.writeStartElement("SimpleField");
                    xMLStreamWriter.writeAttribute("name", "Colour");
                    xMLStreamWriter.writeAttribute("type", "string");
                    xMLStreamWriter.writeEndElement();

                    xMLStreamWriter.writeEndElement();

                    int alphCount = 0;
                    int indexCount = 0;

                    String[] alphabet = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
                    String[] indexes = new String[]{"#a", "#b", "#c", "#d", "#e", "#f", "#g", "#h", "#i", "#j", "#k", "#l",
                        "#m", "#n", "#o", "#p", "#q", "#r", "#s", "#t", "#u", "#v", "#w", "#x", "#y", "#z"};

                    while (rsColor.next()) {

                        String color = rsColor.getString("map_shape_style");

                        xMLStreamWriter.writeStartElement("Style");
                        xMLStreamWriter.writeAttribute("id", alphabet[alphCount]);
                        xMLStreamWriter.writeStartElement("LineStyle");
                        xMLStreamWriter.writeStartElement("color");
                        xMLStreamWriter.writeCharacters("00000000");

                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeEndElement();

                        xMLStreamWriter.writeStartElement("PolyStyle");
                        xMLStreamWriter.writeStartElement("color");

                        String kmlColor = htmlToKml(color);

                        xMLStreamWriter.writeCharacters(kmlColor);
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeEndElement();

                        xMLStreamWriter.writeEndElement();

                        alphCount++;

                    }

                    rsKml = kmlPstmt.executeQuery();

                    while (rsKml.next()) {

                        String map = rsKml.getString("st_askml");

                        xMLStreamWriter.writeStartElement("Placemark");
                        xMLStreamWriter.writeStartElement("name");
                        xMLStreamWriter.writeCharacters("#00FF00");
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeStartElement("description");
                        xMLStreamWriter.writeCharacters("#00FF00");
                        xMLStreamWriter.writeEndElement();

                        xMLStreamWriter.writeStartElement("ExtendedData");
                        xMLStreamWriter.writeStartElement("SchemaData");
                        xMLStreamWriter.writeAttribute("schemaUrl", "#OGRGeoJSON");

                        xMLStreamWriter.writeStartElement("SimpleData");
                        xMLStreamWriter.writeAttribute("name", "Name");
                        xMLStreamWriter.writeCharacters("#00FF00");
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeStartElement("SimpleData");
                        xMLStreamWriter.writeAttribute("name", "Description");
                        xMLStreamWriter.writeCharacters("0");
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeStartElement("SimpleData");
                        xMLStreamWriter.writeAttribute("name", "Colour");
                        xMLStreamWriter.writeCharacters("MULTIPOLYGON");
                        xMLStreamWriter.writeEndElement();

                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeEndElement();

                        xMLStreamWriter.writeStartElement("styleUrl");
                        xMLStreamWriter.writeCharacters(indexes[indexCount]);
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeCData(map);

                        xMLStreamWriter.writeEndElement();

                        indexCount++;

                    }

                    xMLStreamWriter.writeEndDocument();

                    xMLStreamWriter.flush();
                    xMLStreamWriter.close();

                    String rawXmlString = stringWriter.getBuffer().toString();

                    stringWriter.close();

                    String noCdata = rawXmlString.replaceAll("<!\\[CDATA\\[", "");
                    xmlString = noCdata.replaceAll("]]>", "");

                } catch (Exception e) {

                    e.printStackTrace();

                }

                //pstmt.close();
            }

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {

            closeKmlConnection();

        }
        return xmlString;

    }

    public void insertLastUpdate(String tenantId) {

        try {

            createSqlConnection();
            pstmt = conn.prepareStatement(GET_LAST_UPDATE);
            pstmt.setInt(1, Integer.parseInt(tenantId));

            rs = pstmt.executeQuery();
            createSqliteConnection();
            System.out.println("Setting last update for tenant: " + tenantId);        
            while (rs.next()) {

                String timestamp = rs.getString("last_update");
            System.out.println("Setting last update for tenant: " + tenantId + " with timestamp " + timestamp);        
                
                sqlitePstmt = sqliteConn.prepareStatement(INSERT_LAST_UPDATE);
                sqlitePstmt.setString(1, timestamp);
                sqlitePstmt.execute();
            System.out.println("Setting last update for tenant: " + tenantId + " with timestamp " + timestamp + " complete");        

            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            //closeSqliteConnection();

        }
    }

    public String htmlToKml(String color) {

        String alpha = "FF";
        String rawColor = color.substring(1, color.length());
        String rr = rawColor.substring(0, 2);
        String gg = rawColor.substring(2, 4);
        String bb = rawColor.substring(4, 6);
        String kmlColor = alpha + bb + gg + rr;

        return kmlColor;

    }

    public void zipper(String uuid){
    
    byte[] buffer = new byte[1024];
    	
    	try{
    		
    		FileOutputStream fos = new FileOutputStream("/home/pg_ftp_user/provisioned_databases/"+uuid+".zip");
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry(uuid);
    		zos.putNextEntry(ze);
    		FileInputStream in = new FileInputStream("/home/pg_ftp_user/provisioned_databases/"+uuid);
   	   
    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		in.close();
    		zos.closeEntry();
           
    		//remember close it
    		zos.close();
          
    		System.out.println("Done");

    	}catch(IOException ex){
    	   ex.printStackTrace();
    	}
    
    }
    
}
