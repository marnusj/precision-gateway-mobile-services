/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.ArrayList;

/**
 *
 * @author jacobsb01
 */
@WebService(serviceName = "SyncApplicationData")
public class LocationSync extends Base{

    private final String GET_LOCATIONS = "SELECT * FROM locations where id = "
            + "(SELECT location_id from tenant_locations where tenant_id = "
            + "(SELECT tenant_id FROM tenant_users WHERE user_id=? limit 1))";
    private Connection conn;
    private ResultSet rs;
    private PreparedStatement pstmt;
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "syncLocations")
    public ArrayList syncAllLocations(@WebParam(name = "userId") String userId) throws SQLException{
        
        ArrayList<String> locations = new ArrayList<String>();
         try {
            createSqlConnection();
                pstmt = conn.prepareStatement(GET_LOCATIONS);
                pstmt.setString(1, userId);
                rs = pstmt.executeQuery();
                
                while (rs.next()) {
                    String response = createLocationsResponse(rs.getInt("id"),
                            rs.getString("name"),
                            rs.getString("type"),
                            rs.getInt("parent_id"));
                    System.out.println("got data from db");
                    locations.add(response);
                    break;
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
         finally {
            releaseResources();
         }
        return locations;
    }
}
