/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.UUID;
import javax.jws.Oneway;

/**
 *
 * @author jacobsb01
 */
@WebService(serviceName = "ProvisionMobileDatabase")
public class ProvisionMobileDatabase extends Base{

    
    
    @WebMethod(operationName = "generateId")
    public String generateId() {
       String uuid = String.valueOf(UUID.randomUUID());
       return uuid;
    }
    
    @WebMethod(operationName = "checkAvailability")
    public String checkAvailability(@WebParam(name = "uuid") String uuid) {
        return checkDatabaseAvailability(uuid);
    }
    
    
    @WebMethod(operationName = "createDatabase")
    @Oneway
    public void createDatabase(@WebParam(name = "username") String username,@WebParam(name = "password") String password,
            @WebParam(name = "tenantId") String tenantId, @WebParam(name = "uuid") String uuid) {
    
        super.databaseName = uuid;
    
        try {
            System.out.println(username+"    "+password+"   "+tenantId+"   "+uuid);
            createStatusEntry(uuid);
            createSqliteDatabase(uuid);
            createSqliteConnection();
            insertCredentials(username, password);
            insertLocations(tenantId);                
            insertMapData(tenantId);
            prepareKmlMapData(tenantId);
            insertLastUpdate(tenantId);
            zipper(uuid);
            updateStatusEntry(uuid);
            closeSqliteConnection();
                
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            releaseResources();  
        }
    }
}
