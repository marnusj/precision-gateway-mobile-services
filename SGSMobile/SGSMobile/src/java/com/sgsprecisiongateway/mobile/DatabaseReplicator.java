/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgsprecisiongateway.mobile;

import com.google.gson.Gson;
import com.sgsprecisiongateway.mobile.models.Areas;
import com.sgsprecisiongateway.mobile.models.AreasResponse;
import com.sgsprecisiongateway.mobile.models.Locations;
import com.sgsprecisiongateway.mobile.models.LocationsResponse;
import com.sgsprecisiongateway.mobile.models.Shapes;
import com.sgsprecisiongateway.mobile.models.ShapesResponse;
import com.sgsprecisiongateway.mobile.models.ShapeProperties;
import com.sgsprecisiongateway.mobile.models.ShapePropertiesResponse;
import com.sgsprecisiongateway.mobile.models.CachedMapData;
import com.sgsprecisiongateway.mobile.models.CachedMapDataResponse;
import com.sgsprecisiongateway.mobile.models.ShapePropertyType;
import com.sgsprecisiongateway.mobile.models.ShapePropertyTypeResponse;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author jacobsb01
 */
@WebService(serviceName = "Replicator")
public class DatabaseReplicator extends Base{
    
    private static final String GET_LOCATIONS = "SELECT * FROM locations WHERE id IN (SELECT location_id FROM tenant_locations WHERE tenant_id = ?) AND parent_id = ?";
    private static final String GET_PARENT_LOCATION = "SELECT * FROM locations WHERE id IN (SELECT location_id FROM tenant_locations WHERE tenant_id = ?) AND parent_id IS NULL";
    private static final String GET_AREAS = "SELECT * FROM area WHERE location_id = ?";
    private static final String GET_SHAPES = "SELECT id, st_astext(shape_definition) AS geojson, shape_description, area_id FROM shapes WHERE area_id = ?";
    private static final String GET_SHAPE_PROPERTIES = "SELECT * FROM shape_properties WHERE shape_id IN (SELECT id FROM shapes WHERE area_id = ?)";
    private static final String GET_CACHED_MAP_DATA = "SELECT id, st_astext(map_shape_data) AS geojson, map_shape_style, area_id, shape_property_type_id FROM cached_map_data WHERE area_id = ?";
    private static final String GET_SHAPE_PROPERTY_TYPE = "SELECT * FROM shape_property_type";
    
    private Statement stmt;
    private ResultSet rs;
    private ResultSet rsColor;
    
    private PreparedStatement pstmt;
    
    @WebMethod(operationName = "getAreas")
    public String getAreas(@WebParam(name = "locationId") String locationId) {
    
        AreasResponse areasResponse = new AreasResponse();
        Areas areas = null;
        
        try {

                
                createSqlConnection();
                
                
                pstmt = conn.prepareStatement(GET_AREAS);
                pstmt.setInt(1, Integer.parseInt(locationId));

                rs = pstmt.executeQuery();
                
                while (rs.next()) {
                    
                    areas = new Areas();
                    
                    areas.setId(String.valueOf(rs.getInt("id")));
                    areas.setDescription(rs.getString("description"));
                    areas.setOriginalLink(rs.getString("original_link"));
                    areas.setNumberOfShapes(String.valueOf(rs.getInt("number_of_shapes")));
                    areas.setAreaType(rs.getString("area_type"));
                    areas.setLastModified(rs.getString("last_modified"));
                    areas.setPeriod(rs.getString("period"));
                    areas.setActive(rs.getString("active"));
                    areas.setTenantId(String.valueOf(rs.getInt("tenant_id")));
                    areas.setLocationId(String.valueOf(rs.getInt("location_id")));
                    areas.setLatLong(rs.getString("lat_long"));
                    
                    areasResponse.setAreas(areas);
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();        
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(areasResponse);
        return json;
    }
    
    @WebMethod(operationName = "getLocations")
    public String getLocations(@WebParam(name = "tenantId") String tenantId,@WebParam(name = "parentId") String parentId) {
    
        LocationsResponse locationsResponse = new LocationsResponse();
        Locations location = null;
        Integer userIdInteger = Integer.parseInt(tenantId);
        
        try {

            if (userIdInteger != -1) {
                createSqlConnection();
                if (parentId.equals("null")){
                    pstmt = conn.prepareStatement(GET_PARENT_LOCATION);
                } 
                else {
                    pstmt = conn.prepareStatement(GET_LOCATIONS);
                    pstmt.setInt(2, Integer.parseInt(parentId));
                }
                pstmt.setInt(1, Integer.parseInt(tenantId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    
                    location = new Locations();
                    location.setId(String.valueOf(rs.getInt("id")));
                    location.setName(rs.getString("name"));
                    location.setType(rs.getString("type"));
                    location.setParent_id(String.valueOf(rs.getInt("parent_id")));
                    locationsResponse.setLocations(location);
                }
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();        
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(locationsResponse);
        
        return json;
    }
    
    
     @WebMethod(operationName = "getShapes")
     public String getShapes(@WebParam(name = "areaId") String areaId) {
    
        ShapesResponse shapesResponse = new ShapesResponse();
        Shapes shapes = null;
        
        try {
                
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_SHAPES);
                pstmt.setInt(1, Integer.parseInt(areaId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    shapes = new Shapes();
                    shapes.setId(String.valueOf(rs.getInt("id")));
                    shapes.setShapeDefinition(rs.getString("geojson"));
                    shapes.setShapeDescription(rs.getString("shape_description"));
                    shapes.setAreaId(String.valueOf(rs.getInt("area_id")));
                    shapesResponse.setShapes(shapes);
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();        
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(shapesResponse);
        
        return json;
    }
     
     
     @WebMethod(operationName = "getShapeProperties")
     public String getShapeProperties(@WebParam(name = "areaId") String areaId) {
    
        ShapePropertiesResponse shapesPropertiesResponse = new ShapePropertiesResponse();
        ShapeProperties shapeProperties = null;
        try {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_SHAPE_PROPERTIES);
                pstmt.setInt(1, Integer.valueOf(areaId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    shapeProperties = new ShapeProperties();
                    shapeProperties.setId(String.valueOf(rs.getInt("id")));
                    shapeProperties.setName(rs.getString("name"));
                    shapeProperties.setValue(rs.getString("value"));
                    shapeProperties.setShapeId(String.valueOf(rs.getInt("shape_id")));
                    shapeProperties.setShapePropertyTypeId(String.valueOf(rs.getInt("shape_property_type_id")));
                    shapesPropertiesResponse.setShapeProperties(shapeProperties);
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();        
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(shapesPropertiesResponse);
        
        return json;
    }
     
     @WebMethod(operationName = "getCachedMapData")
     public String getCachedMapData(@WebParam(name = "areaId") String areaId) {
    
        CachedMapDataResponse cachedMapDataResponse = new CachedMapDataResponse();
        CachedMapData cachedMapData = null;
        
        try {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_CACHED_MAP_DATA);
                pstmt.setInt(1, Integer.parseInt(areaId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    cachedMapData = new CachedMapData();
                    cachedMapData.setId(String.valueOf(rs.getInt("id")));
                    cachedMapData.setMapShapeData(rs.getString("geojson"));
                    cachedMapData.setMapShapeStyle(rs.getString("map_shape_style"));
                    cachedMapData.setAreaId(String.valueOf(rs.getInt("area_id")));
                    cachedMapData.setShapePropertyTypeId(String.valueOf(rs.getInt("shape_property_type_id")));
                    cachedMapDataResponse.setCachedMapData(cachedMapData);
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();        
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(cachedMapDataResponse);
        
        return json;
    }
     
      @WebMethod(operationName = "getShapePropertyType")
     public String getShapePropertyType() {
    
        ShapePropertyTypeResponse shapePropertyTypeResponse = new ShapePropertyTypeResponse();
        ShapePropertyType shapePropertyType = null;
        
        try {
                createSqlConnection();
                pstmt = conn.prepareStatement(GET_SHAPE_PROPERTY_TYPE);
                rs = pstmt.executeQuery();
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {
            releaseResources();        
        }
        Gson gson = new Gson(); 
        String json = gson.toJson(shapePropertyTypeResponse);
        
        return json;
    }
}
